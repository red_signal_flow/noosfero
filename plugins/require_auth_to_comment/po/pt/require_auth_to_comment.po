# translation of noosfero.po to
# Krishnamurti Lelis Lima Vieira Nunes <krishna@colivre.coop.br>, 2007.
# noosfero - Brazilian Portuguese translation
# Copyright (C) 2007,
# Forum Brasileiro de Economia Solidaria <http://www.fbes.org.br/>
# Copyright (C) 2007,
# Ynternet.org Foundation <http://www.ynternet.org/>
# This file is distributed under the same license as noosfero itself.
# Joenio Costa <joenio@colivre.coop.br>, 2008.
#
#
msgid ""
msgstr ""
"Project-Id-Version: 1.1-166-gaf47713\n"
"POT-Creation-Date: 2015-06-01 17:26-0300\n"
"PO-Revision-Date: 2014-12-18 18:40-0200\n"
"Last-Translator: Luciano Prestes Cavalcanti <lucianopcbr@gmail.com>\n"
"Language-Team: Portuguese <https://hosted.weblate.org/projects/noosfero/"
"noosfero/pt/>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 2.0\n"

#: plugins/require_auth_to_comment/lib/require_auth_to_comment_plugin.rb:12
msgid "Requires users to authenticate in order to post comments."
msgstr "Exige que os usuários se autentiquem antes de postar coentários."

#: plugins/require_auth_to_comment/views/require_auth_to_comment_plugin_admin/index.html.erb:1
msgid "Require auth to comment Settings"
msgstr ""

#: plugins/require_auth_to_comment/views/require_auth_to_comment_plugin_admin/index.html.erb:8
msgid "Hide button"
msgstr ""

#: plugins/require_auth_to_comment/views/require_auth_to_comment_plugin_admin/index.html.erb:11
msgid "Display login popup"
msgstr ""

#: plugins/require_auth_to_comment/views/require_auth_to_comment_plugin_admin/index.html.erb:17
msgid "Save"
msgstr ""

#: plugins/require_auth_to_comment/views/profile-editor-extras.html.erb:1
msgid "Comments"
msgstr "Comentários"

#: plugins/require_auth_to_comment/views/profile-editor-extras.html.erb:3
msgid "Accept comments from unauthenticated users"
msgstr "Aceitar comentários de usuários não autenticados"
